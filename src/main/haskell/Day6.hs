module Day6 where

import Prelude
import qualified Data.Set as Set
import Data.Vector.Unboxed (Vector)
import qualified Data.Vector.Unboxed as V

updateF :: Vector Int -> (Int, Int -> Int) -> Vector Int
updateF v (i, f) = v V.// [(i, f (v V.! i))]

redistribute :: Vector Int -> Vector Int
redistribute v = foldl (\newV i -> updateF newV (i `mod` V.length v, (+ 1))) initialV indexes
  where maxIndex = V.maxIndex v
        maxElem = v V.! maxIndex
        initialV = v V.// [(maxIndex, 0)]
        indexes =  [maxIndex + 1.. maxIndex + maxElem]

solve :: Vector Int -> (Int, Vector Int)
solve = loop 0 Set.empty
  where loop c s v
          | Set.member (show v) s = (c, v)
          | otherwise = loop (c + 1) (Set.insert (show v) s) (redistribute v)

main :: IO ()
main = let input = V.fromList [0, 5, 10, 0, 11, 14, 13, 4, 11, 8, 8, 7, 1, 4, 12, 11]
           (part1, lastV) = solve input
           (part2, _) = solve lastV
       in do putStrLn ("Part1: " ++ show part1)
             putStrLn ("Part2: " ++ show part2)