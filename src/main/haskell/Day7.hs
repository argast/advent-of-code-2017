{-# LANGUAGE QuasiQuotes #-}

module Day7 where

import Prelude
import Data.List.Split
import qualified Data.Set as Set
import Debug.Trace
import Text.Regex.PCRE.Heavy

data Block = Block { name:: String
                   , weight :: Int
                   , subBlocks :: [String]
                   } deriving (Show)

toBlock :: (String, [String]) -> Block
toBlock (_, [name, weight, "", ""]) = Block name (read weight :: Int) []
toBlock (_, [name, weight, _, blocks]) = Block name (read weight :: Int) (splitOn ", " blocks)
toBlock _ = Block "" 0 [""]

parse :: String -> [Block]
parse s = map toBlock (scan [re|(.*) \((.*)\)( -> )?(.*)?|] s)

part1 :: [Block] -> String
part1 blocks =  Set.elemAt 0 (Set.difference all sub)
  where all = Set.fromList (map name blocks)
        sub = Set.fromList (concatMap subBlocks blocks)

main :: IO ()
main = do text <- readFile "src/main/resources/Day7.txt"
          let blocks = parse text
          putStrLn ("Part1: " ++ show (part1 blocks))



