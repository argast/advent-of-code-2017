{-# LANGUAGE QuasiQuotes #-}

module Day13 where

import Prelude
import Text.Regex.PCRE.Heavy
import Data.List

data Scanner = Scanner { depth :: Int
                       , range :: Int
                       } deriving Show

caught :: Scanner -> Int -> Bool
caught scanner delay = (depth scanner + delay) `mod` (2 * (range scanner - 1)) == 0

parse :: String -> [Scanner]
parse s = map parseScanner (scan [re|(.*): (.*)|] s)

parseScanner :: (String, [String]) -> Scanner
parseScanner (_, [depth, range]) = Scanner parseDepth parseRange
  where parseDepth = read depth :: Int
        parseRange = read range :: Int

part1 :: [Scanner] -> Int
part1 scanners = sum $ map (\s -> depth s * range s) $ filter (`caught` 0) scanners

part2 :: [Scanner] -> Maybe Int
part2 scanners = elemIndex False $ map (\delay -> any (`caught` delay) scanners) [0..]

main :: IO ()
main = do text <- readFile "src/main/resources/Day13.txt"
          let scanners = parse text
          putStrLn ("Part1: " ++ show (part1 scanners))
          putStrLn ("Part2: " ++ show (part2 scanners))


