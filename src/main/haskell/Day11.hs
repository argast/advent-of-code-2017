module Day11 where

import Prelude
import Data.List.Split

move :: (Int, Int) -> String -> (Int, Int)
move (x, y) "n" = (x, y + 2)
move (x, y) "ne" = (x + 1, y + 1)
move (x, y) "se" = (x + 1, y - 1)
move (x, y) "s" = (x, y - 2)
move (x, y) "sw" = (x - 1, y - 1)
move (x, y) "nw" = (x - 1, y + 1)

distance :: (Int, Int) -> Int
distance (x, y)
          | absX < absY = absX + (absY - absX) `div` 2
          | otherwise = absX
          where absX = abs x
                absY = abs y

positions :: [String] -> [(Int, Int)]
positions = scanl move (0, 0)

part1 :: [String] -> Int
part1 s = distance $ last (positions s)

part2 :: [String] -> Int
part2 s = maximum $ map distance (positions s)

main :: IO ()
main = do text <- readFile "src/main/resources/Day11.txt"
          let moves = splitOn "," text
          putStrLn ("Part1: " ++ show (part1 moves))
          putStrLn ("Part2: " ++ show (part2 moves))

