{-# LANGUAGE QuasiQuotes #-}

module Day8 where

import Prelude
import qualified Data.Map.Strict as Map
import Debug.Trace
import Text.Regex.PCRE.Heavy

type Memory = Map.Map String Int

data Instruction = Instruction { operation :: Memory -> Memory
                               , condition  :: Memory -> Bool
                               }

parseOperation :: String -> Int -> String -> (Memory -> Memory)
parseOperation op v n = Map.insertWith (\ _ ov -> f ov v) n (f 0 v)
  where f x v
          | op == "inc" = x + v
          | op == "dec" = x - v

parseCondition :: String -> Int -> String -> (Memory -> Bool)
parseCondition op v n m = f valueFromMemory v
  where valueFromMemory = Map.findWithDefault 0 n m
        f
          | op == "<" = (<)
          | op == ">" = (>)
          | op == ">=" = (>=)
          | op == "<=" = (<=)
          | op == "==" = (==)
          | op == "!=" = (/=)

parseInstruction :: (String, [String]) -> Instruction
parseInstruction (_, [name, op, value, condName, cond, condV]) =
  Instruction (parseOperation op (read value :: Int) name) (parseCondition cond (read condV :: Int) condName)

run :: Memory -> Instruction -> Memory
run memory instruction = if condition instruction memory then operation instruction memory else memory

parse :: String -> [Instruction]
parse s = map parseInstruction (scan [re|(.*) (inc|dec) (.*) if (.*) (.*) (.*)|] s)

part1 :: [Instruction] -> Int
part1 instructions =  maxRegister (last $ runProgram instructions)

part2 :: [Instruction] -> Int
part2 instructions =  maximum (map maxRegister (runProgram instructions))

runProgram :: [Instruction] -> [Memory]
runProgram = scanl run Map.empty

maxRegister :: Memory -> Int
maxRegister = Map.foldl max 0

main :: IO ()
main = do text <- readFile "src/main/resources/Day8.txt"
          let instructions = parse text
          putStrLn ("Part1: " ++ show (part1 instructions))
          putStrLn ("Part2: " ++ show (part2 instructions))
