{-# LANGUAGE QuasiQuotes #-}

module Day12 where

import Prelude
import Text.Regex.PCRE.Heavy
import Data.List
import Data.List.Split
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

type Pipes = Map.Map Int [Int]

parse :: String -> Pipes
parse s = Map.fromList $ map parsePipe (scan [re|(.*) <-> (.*)|] s)

parsePipe :: (String, [String]) -> (Int, [Int])
parsePipe (_, [id, connected]) = (parseId, parseConnected)
  where parseId = read id :: Int
        parseConnected = map (\s -> read s :: Int) $ splitOn ", " connected

groups :: Pipes -> Int -> Set.Set Int
groups pipes i = Set.fromList $ unfoldr (\ l -> if null l then Nothing else Just (head l, tail l ++ ((Map.delete (head l) pipes) Map.! (head l)))) (pipes Map.! i)

part1 pipes = groups pipes 0

--part2 :: Pipes -> Maybe Int
part2 pipes = pipes

main :: IO ()
main = do text <- readFile "src/main/resources/Day12.txt"
          let pipes = parse text
          putStrLn ("Part1: " ++ show (part1 pipes))
         -- putStrLn ("Part2: " ++ show (part2 pipes))


