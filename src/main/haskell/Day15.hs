module Day15 where

import Prelude
import Data.Bits
import Data.Int
import Data.List
import Debug.Trace
import Numeric

mask = (maxBound :: Int64) `shiftR` 47

initialA = 873 :: Int64
initialB = 583 :: Int64

values :: Int64 -> Int64 -> Int64 -> [Int64]
values mult initial divider = drop 1 $ filter (\i -> i `mod` divider == 0) (iterate (\i -> i * mult `mod` 2147483647) initial)

compareLast16 :: Int64 -> Int64 -> Int64
compareLast16 a b
  | a .&. mask == b .&. mask = 1
  | otherwise = 0

countSameNumbers iterations valuesA valuesB = sum $ take iterations (zipWith compareLast16 valuesA valuesB)

part1 :: Int64
part1 = countSameNumbers 40000000 valuesA valuesB
  where valuesA = values 16807 initialA 1
        valuesB = values 48271 initialB 1

part2 :: Int64
part2 = countSameNumbers 5000000 valuesA valuesB
  where valuesA = values 16807 initialA 4
        valuesB = values 48271 initialB 8

main :: IO ()
main = do putStrLn ("Part1: " ++ show part1)
          putStrLn ("Part2: " ++ show part2)


