module Day5 where

import Prelude
import Data.Vector.Unboxed (Vector)
import qualified Data.Vector.Unboxed as V

solve :: (Int -> Int) -> Vector Int -> Int
solve f = loop 0 0
  where  loop count pos i
          | newPos < 0 || newPos >= V.length i = count + 1
          | otherwise = loop (count + 1) newPos newList
          where offset = i V.! pos
                newPos = pos + offset
                newList = i V.// [(pos, i V.! pos + f offset)]

main :: IO ()
main = do text <- readFile "src/main/resources/Day5.txt"
          let input = V.fromList $ map (\s -> read s :: Int) (lines text)
          putStrLn ("Part1: " ++ show (solve (const 1) input))
          putStrLn ("Part2: " ++ show (solve (\o -> if o >= 3 then -1 else 1) input))

