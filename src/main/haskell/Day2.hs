module Day2 where

import Prelude

part1 :: [[Int]] -> Int
part1 ints = sum $ map (\l -> maximum l - minimum l) ints

part2 :: [[Int]] -> Int
part2 ints = sum $ map (uncurry div . head . filter (\p -> uncurry mod p == 0) . pairs) ints
  where pairs l = [(a, b) | a <- l, b <- l, b /= a]

main :: IO ()
main = do text <- readFile "src/main/resources/Day2.txt"
          let input = lines text
          let ints = map (map (\ s -> read s :: Int) . words) input
          putStrLn ("Part1: " ++ show (part1 ints))
          putStrLn ("Part2: " ++ show (part2 ints))


