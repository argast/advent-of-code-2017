module Day3 where

import Prelude
import qualified Data.Map as Map

steps = concat $ zipWith replicate moves possibleSteps
  where possibleSteps = cycle [(1, 0), (0, 1), (-1, 0), (0, -1)]
        moves = concatMap (replicate 2) [1 ..]

addPositions :: (Int, Int) -> (Int, Int) -> (Int, Int)
addPositions p1 p2 = (fst p1 + fst p2, snd p1 + snd p2)

memory = scanl addPositions (0, 0) steps

part1 = abs (fst pos) + abs (snd pos)
  where pos = memory !! 265148

neighbours = [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, -1), (1, -1), (-1, 1)]

part2 = (head . dropWhile (< 265148)) memoryTest
  where memoryTest = map fst $ scanl neighboursSum (1, Map.singleton (0, 0) 1) (drop 1 memory)
        neighboursSum (v, m) pos = (calculatedSum, Map.insert pos calculatedSum m)
          where calculatedSum = sum $ map (neighbourValue m . addPositions pos) neighbours
        neighbourValue = flip $ Map.findWithDefault 0

main :: IO ()
main = do putStrLn ("Part1: " ++ show part1)
          putStrLn ("Part2: " ++ show part2)

