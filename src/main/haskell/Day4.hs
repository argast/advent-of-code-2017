module Day4 where

import Prelude
import Data.List
import qualified Data.Map as Map

part1 :: [String] -> Int
part1 input = length $ filter (== 1) $ map (groupItems . words) input
  where groupItems words = maximum $ Map.elems $ Map.map length (Map.fromListWith (++) [(w, [w]) | w <- words])

part2 input = length $ filter (== 1) $ map (groupItems . words) input
  where groupItems words = maximum $ Map.elems $ Map.map length (Map.fromListWith (++) [(sort w, [w]) | w <- words])

main :: IO ()
main = do text <- readFile "src/main/resources/Day4.txt"
          let input = lines text
          putStrLn ("Part1: " ++ show (part1 input))
          putStrLn ("Part2: " ++ show (part2 input))

