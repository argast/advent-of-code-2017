{-# LANGUAGE QuasiQuotes #-}

module Day16 where

import Prelude
import Text.Regex.PCRE.Heavy
import Data.List
import Data.List.Split
import Data.Maybe

type Move = String -> String

spinRegex = [re|s(.*)|]
exchangeRegex = [re|x(.*)/(.*)|]
partnerRegex = [re|p(.*)/(.*)|]

spin :: Int -> Move
spin i s = (take l . drop (l - i)) $ cycle s
  where l = length s

exchange :: Int -> Int -> Move
exchange i j s = beginning ++ [xj] ++ middle ++ [xi] ++ end
  where (beginning, xi : rest) = splitAt ii s
        (middle, xj : end) = splitAt (jj - ii - 1) rest
        ii = min i j
        jj = max i j

partner :: Char -> Char -> Move
partner n m s = exchange (fromJust $ elemIndex n s) (fromJust $ elemIndex m s) s

parse :: [String] -> [Move]
parse = map parseMove
  where parseMove s
          | s =~ spinRegex = parseSpin $ head $ scan spinRegex s
          | s =~ exchangeRegex = parseExchange $ head $ scan exchangeRegex s
          | s =~ partnerRegex = parsePartner $ head $ scan partnerRegex s

parseSpin :: (String, [String]) -> Move
parseSpin (_, [n]) = spin (read n :: Int)

parseExchange :: (String, [String]) -> Move
parseExchange (_, [i, j]) = exchange (read i :: Int) (read j :: Int)

parsePartner :: (String, [String]) -> Move
parsePartner (_, [n, m]) = partner (head n) (head m)

dance :: [Move] -> String -> String
dance moves s = foldl (\s m -> m s) s moves

part1 :: [Move] -> String -> String
part1 moves s = iterate (dance moves) s !! 1

part2 :: [Move] -> String -> String
part2 moves s = positions !! ((1000000000 `mod` (cycleLength + 1)) - 1)
  where positions = drop 1 $ iterate (dance moves) s
        cycleLength = fromJust $ elemIndex s positions

main :: IO ()
main = do text <- readFile "src/main/resources/Day16.txt"
          let moves = parse $ splitOn "," text
          let initial = ['a'..'p']
          putStrLn ("Part1: " ++ show (part1 moves initial))
          putStrLn ("Part2: " ++ show (part2 moves initial))


