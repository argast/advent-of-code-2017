  {-# LANGUAGE QuasiQuotes #-}

module Day9 where

import Prelude
import Text.Regex.PCRE.Heavy

removeCancelled s = gsub [re|!.|] "" s :: String
removeGarbage s = gsub [re|<.*?>|] "" s :: String
removeWithinGarbage s = gsub [re|<.*?>|] "<>" s :: String

clean1 = removeWithinGarbage . removeCancelled

score :: String -> Int
score s = snd (foldl f (0, 0) s)
  where f (level, score) c
         | c == '{' = (level + 1, score)
         | c == '}' = (level - 1, score + level)
         | otherwise = (level, score)

part1 :: String -> Int
part1 = score . removeGarbage . removeCancelled

part2 :: String -> Int
part2 s = (length . removeCancelled) s - (length . removeWithinGarbage . removeCancelled) s

main :: IO ()
main = do text <- readFile "src/main/resources/Day9.txt"
          putStrLn ("Part1: " ++ show (part1 text))
          putStrLn ("Part2: " ++ show (part2 text))

