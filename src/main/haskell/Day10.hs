  {-# LANGUAGE QuasiQuotes #-}

module Day10 where

import Prelude
import Data.List.Split
import Data.Char
import Data.Bits
import Numeric
import Debug.Trace

totalLength = 256

twist :: [Int] -> (Int, Int) -> [Int]
twist ints (length, skip) = (take totalLength . drop (skip + length)) (cycle reversed)
  where split = splitAt length ints
        reversed = reverse (fst split) ++ snd split

cycles :: [Int] -> [[Int]]
cycles input = scanl twist [0..(totalLength - 1)] (cycle input `zip` [0..])

normalized input i = (take totalLength . drop pos) $ cycle (cycles input !! i)
  where pos = totalLength - abs ((sum . take i) (zipWith (+) (cycle input) [0 ..])) `mod` totalLength

part1 :: [Int] -> Int
part1 input = hash !! 0 * hash !! 1
  where hash = normalized input (length input)

part2 input = hexString hash
  where hash = normalized input (length input * 64)
        xorSum = foldl1 xor
        xored h = map xorSum (chunksOf 16 h)
        hexString h = foldr toHex "" (xored h)

toHex i s
      | length hexStr == 1 = "0" ++ hexStr
      | otherwise = hexStr
      where hexStr = showHex i s

main :: IO ()
main = do text <- readFile "src/main/resources/Day10.txt"
          let input = map (\s -> read s :: Int) (splitOn "," text)
          let hashInput = map ord text ++ [17, 31, 73, 47, 23]
          putStrLn ("Part1: " ++ show (part1 input))
          putStrLn ("Part2: " ++ show (part2 hashInput))

