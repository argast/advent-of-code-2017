module Day17 where

import Prelude
import Data.List
import Data.List.Split

spin :: [Int] -> [Int]
spin ints =  l : (take l . drop (input + 1)) (cycle ints)
  where input = 344
        l = length ints

part1 = iterate spin [0] !! 2017 !! 1

calculate0 (pos0, after0) i
  | pos0 == pos = (i, i)
  | pos0 < pos = (i - (pos - pos0), after0)
  | otherwise = (pos0 - pos, after0)
  where pos = 344 `mod` i
part2 = snd $  foldl calculate0 (0, 0) [1..50000000]

main :: IO ()
main = do putStrLn ("Part1: " ++ show part1)
          putStrLn ("Part2: " ++ show part2)


