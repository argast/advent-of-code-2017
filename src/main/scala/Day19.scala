import scala.io.Source

object Day19 extends App {

  val input = Source.fromResource("Day19.txt").getLines()

  val map: Array[Array[Char]] = input.toArray.map(_.toCharArray)

  sealed abstract class Direction(val dx: Int, val dy: Int)
  case object Left extends Direction(-1, 0)
  case object Right extends Direction(1, 0)
  case object Up extends Direction(0, -1)
  case object Down extends Direction(0, 1)


  def turn(x: Int, y: Int, d: Direction): Direction = {
    if (d.dx != 0) {
      if (map(y - 1)(x) != ' ') Up else Down
    } else {
      if (map(y)(x - 1) != ' ') Left else Right
    }
  }

  def steps: Stream[Char] = {

    def loop(x: Int, y: Int, d: Direction): Stream[(Int, Int, Direction)] =
      if (map(y)(x) == ' ') Stream.empty
      else (x, y, d) #:: (map(y)(x) match {
        case '+' =>
          val newDirection = turn(x, y, d)
           loop(x + newDirection.dx, y + newDirection.dy, newDirection)
        case _ => loop(x + d.dx, y + d.dy, d)
      })

    loop(map(0).indexOf('|'), 0, Down).map { case (x, y, _) => map(y)(x) }
  }

  println("Part1: " + steps.filter(_.isLetter).mkString)
  println("Part2: " + steps.length)
}
