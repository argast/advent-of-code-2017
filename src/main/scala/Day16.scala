import scala.io.Source

object Day16 extends App {

  val initial = "abcdefghijklmnop"
  val length = initial.length

  sealed trait Move {
    def dance(s: String): String
  }

  case class Spin(i: Int) extends Move {
    override def dance(s: String): String = s.takeRight(i) ++ s.take(s.length - i)
  }
  case class Exchange(i: Int, j: Int) extends Move {
    override def dance(s: String): String = s.updated(i, s.charAt(j)).updated(j, s.charAt(i))
  }
  case class Partner(a: Char, b: Char) extends Move {
    override def dance(s: String): String = s.updated(s.indexOf(a), b).updated(s.indexOf(b), a)
  }

  val input = Source.fromResource("Day16.txt").getLines().mkString
  val input1 = "s1,x3/4,pe/b"

  val spinRegex = "s(\\d*)".r
  val exchangeRegex = "x(\\d*)/(\\d*)".r
  val partnerRegex = "p(.)/(.)".r

  def parse(input: String) = input.split(",").map {
    case spinRegex(i) => Spin(i.toInt)
    case exchangeRegex(i, j) => Exchange(i.toInt, j.toInt)
    case partnerRegex(a, b) => Partner(a(0), b(0))
  }.toVector

  val moves = parse(input)

  def dance(initial: String) = moves.foldLeft(initial)((s, m) => m.dance(s))

  lazy val states: Stream[String] = initial #:: states.map(dance)

  val cycleLength = states.indexOf(states.head, 1)

  println("Part1: " + dance(initial))
  println("Part2: " + Stream.continually(states.take(cycleLength)).flatten.drop(1000000000).head)
}
