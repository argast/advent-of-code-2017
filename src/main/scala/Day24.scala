import scala.io.Source

object Day24 extends App {

  val input =  Source.fromResource("Day24.txt").mkString
  val input1 = """0/2
                 |2/2
                 |2/3
                 |3/4
                 |3/5
                 |0/1
                 |10/1
                 |9/10""".stripMargin


  case class Component(id: Int, p1: Int, p2: Int)

  val components = input.split("\n").zipWithIndex.map { case (s, i) =>
    val (c1, c2) = s.splitAt(s.indexOf('/'))
    Component(i, c1.toInt, c2.drop(1).toInt)
  }

  def bridges(components: Array[Component]) = {

    def valid(prev: Int): PartialFunction[Component, Component] = {
      case Component(i, l, r) if l == prev =>  Component(i, l, r)
      case Component(i, l, r) if r == prev =>  Component(i, r, l)
    }

    def loop(rest: Array[Component], current: Array[Int], bridges: Array[Array[Int]]): Array[Array[Int]] = {
      val compatible = if (current.nonEmpty) rest.collect(valid(current.last)) else rest.collect(valid(0))
      if (compatible.isEmpty) bridges :+ current
      else compatible.flatMap { r =>
        val newCurrent = current.clone :+ r.p1 :+ r.p2
        loop(rest.filterNot(_.id == r.id), newCurrent, bridges :+ newCurrent)
      }
    }

    loop(components, Array.empty, Array.empty)
  }

  def strength(b: Array[Int]) = b.sum

  val allBridges = bridges(components)
  println("Part1: " + allBridges.map(strength).max)
  println("Part2: " + allBridges.groupBy(_.length).maxBy(_._1)._2.map(strength).max)

}
