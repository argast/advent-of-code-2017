
import scala.io.Source

object Day11 extends App {

 val input = Source.fromResource("Day11.txt").getLines().next().split(",").toVector

  case class Position(x: Int, y: Int) {
    def move(step: String): Position = step match {
      case "n" => Position(x, y + 2)
      case "ne" => Position(x + 1, y + 1)
      case "se" => Position(x + 1, y - 1)
      case "s" => Position(x, y - 2)
      case "sw" => Position(x - 1, y - 1)
      case "nw" => Position(x - 1, y + 1)
    }
  }

  val allPositions = input.scanLeft(Position(0, 0))(_.move(_))

  def calculateDistance(p: Position) = {
    val absX = Math.abs(p.x)
    val absY = Math.abs(p.y)
    if (absX < absY) absX + (absY - absX) / 2
    else absY + (absX - absY)
  }

  println("Part1: " + calculateDistance(allPositions.last))
  println("Part2: " + allPositions.map(calculateDistance).max)

}
