import scala.io.Source

object Day20 extends App {

  case class Input(x: Long, y: Long, z: Long)
  object Input {
    def fromString(s: String): Input = s.split(",") match {
      case Array(x, y, z) => Input(x.toLong, y.toLong, z.toLong)
    }
  }

  class Particle(val id: Long, val p: Input, val v: Input, val a: Input) {
    override def toString: String = p.toString
  }

  val input = Source.fromResource("Day20.txt").mkString

  val regex = "p=<(.*)>, v=<(.*)>, a=<(.*)>".r
  def parse(input: String) = input.split("\n").zipWithIndex.map {
    case (regex(p, v, a), id) =>
      new Particle(id, Input.fromString(p), Input.fromString(v), Input.fromString(a))
  }

  val particles = parse(input)

  def speedLongTerm(p: Particle) = {
    // particle with lowest long term speed will be closest
    val i = 10000000
    val a = Math.abs(p.v.x + i * p.a.x) + Math.abs(p.v.y + i * p.a.y) + Math.abs(p.v.z + i * p.a.z)
    p.id -> a
  }

  def collisions(p: Vector[Particle]) = {

    def vSum(v0: Long, a0: Long, n: Long): Long = v0 * n + (a0 + a0 * n) * n / 2

    def position(p: Particle, n: Int): (Long, Long, Long) = {
      val VxSum = vSum(p.v.x, p.a.x, n)
      val VySum = vSum(p.v.y, p.a.y, n)
      val VzSum = vSum(p.v.z, p.a.z, n)
      (p.p.x + VxSum, p.p.y + VySum, p.p.z + VzSum)
    }

    def loop(currentParticles: Vector[Particle], i: Int): Vector[Particle] = {
      if (i == 1000) currentParticles
      else loop(currentParticles.groupBy(p => position(p, i)).filterNot(_._2.size > 1).values.flatten.toVector, i + 1)
    }
    loop(p, 1)
  }

  println("Part1: " + particles.map(speedLongTerm).minBy(a => Math.abs(a._2))._1)
  println("Part2: " + collisions(particles.toVector).length)
}
