import scala.collection.mutable

object Day3 extends App {

  case class Memory(n: Int, x: Int, y: Int)
  case class Step(x: Int, y: Int)

  val possibleSteps = Stream.continually(List(Step(1, 0), Step(0, 1), Step(-1, 0), Step(0, -1))).flatten
  val numberOfMoves = Stream.from(1).flatMap(i => Stream.fill(2)(i))

  val steps = numberOfMoves.zip(possibleSteps).flatMap {
    case (i, step) => Stream.fill(i)(step)
  }

  val memory = steps.scanLeft(Memory(1, 0, 0)) { case (Memory(n, x, y), Step(sx, sy)) =>
    Memory(n + 1, x + sx, y + sy)
  }

  val input = 265149

  println("Part1: " + memory.find(_.n == input).map(m => Math.abs(m.x) + Math.abs(m.y)).get)

  val neighboursSum = mutable.Map((0, 0) -> 1).withDefaultValue(0)
  val neighbours = List((0, 1), (0, -1), (1, 0), (1, 1), (1, -1), (-1, -1), (-1, 0), (-1, 1))

  val nSum = memory.drop(1).map { case Memory(_, x, y) =>
    val sum = neighbours.map(n => neighboursSum((x + n._1, y + n._2))).sum
    neighboursSum += (x, y) -> sum
    sum
  }
  println("Part2: " + nSum.dropWhile(_ < input).head)
}
