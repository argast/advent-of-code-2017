import scala.util.Try

object Day18 extends App {

  val input = """set i 31
                |set a 1
                |mul p 17
                |jgz p p
                |mul a 2
                |add i -1
                |jgz i -2
                |add a -1
                |set i 127
                |set p 316
                |mul p 8505
                |mod p a
                |mul p 129749
                |add p 12345
                |mod p a
                |set b p
                |mod b 10000
                |snd b
                |add i -1
                |jgz i -9
                |jgz a 3
                |rcv b
                |jgz b -1
                |set f 0
                |set i 126
                |rcv a
                |rcv b
                |set p a
                |mul p -1
                |add p b
                |jgz p 4
                |snd a
                |set a b
                |jgz 1 3
                |snd b
                |set f 1
                |add i -1
                |jgz i -11
                |snd a
                |jgz f -16
                |jgz a -19""".stripMargin

  type Registry = Map[String, Long]

  def resolveValue(r: Registry, s: String) = Try(s.toLong).getOrElse(r(s))

  sealed trait Instruction {
    def run(registry: Registry): Registry

  }

  case class Snd(r: String) extends Instruction {
    override def run(registry: Registry): Registry = {
      registry.updated("lastSnd", resolveValue(registry, r)).updated("snd", registry("snd") + 1)
    }
  }
  case class Set(r: String, v: String) extends Instruction {
    override def run(registry: Registry): Registry = registry.updated(r, resolveValue(registry, v))
  }
  case class Add(r: String, v: String) extends Instruction {
    override def run(registry: Registry): Registry = registry.updated(r, registry(r) + resolveValue(registry, v))
  }
  case class Mul(r: String, v: String) extends Instruction {
    override def run(registry: Registry): Registry = registry.updated(r, registry(r) * resolveValue(registry, v))
  }
  case class Mod(r: String, v: String) extends Instruction {
    override def run(registry: Registry): Registry = registry.updated(r, registry(r) % resolveValue(registry, v))
  }
  case class Rcv(r: String) extends Instruction {
    override def run(registry: Registry): Registry = ???
  }
  case class Jgz(cond: String, offset: String) extends Instruction {
    override def run(registry: Registry): Registry = ???
  }

  def parse(input: String): List[Instruction] = input.split("\n").map(_.split(" ") match {
    case Array("snd", r) => Snd(r)
    case Array("set", r, v) => Set(r, v)
    case Array("add", r, v) => Add(r, v)
    case Array("mul", r, v) => Mul(r, v)
    case Array("mod", r, v) => Mod(r, v)
    case Array("rcv", r) => Rcv(r)
    case Array("jgz", r, o) => Jgz(r, o)
  }).toList

  val input1 = """set a 1
                 |add a 2
                 |mul a a
                 |mod a 5
                 |snd a
                 |set a 0
                 |rcv a
                 |jgz a -1
                 |set a 1
                 |jgz a -2""".stripMargin

  val input2 = """snd 1
                 |snd 2
                 |snd p
                 |rcv a
                 |rcv b
                 |rcv c
                 |rcv d""".stripMargin

  val instructions = parse(input)
  val initialRegistry: Registry = Map.empty[String, Long].withDefaultValue(0L)

  def runProgram(instructions: List[Instruction]): Registry = {

    def loop(ind: Int, reg: Registry): Registry = if (ind < 0 || ind > instructions.length) reg
    else instructions(ind) match {
      case Rcv(r)=> if (reg(r) != 0) reg else loop(ind + 1, reg)
      case Jgz(c, o) =>
        if (resolveValue(reg, c) > 0) loop(ind + resolveValue(reg, o).toInt, reg)
        else loop(ind + 1, reg)
      case i => loop(ind + 1, i.run(reg))
    }

    loop(0, initialRegistry)
  }

  def runPrograms(instructions: List[Instruction]): (Registry, Registry) = {

    def cycle(ind: Int, reg: Registry, sBuf: List[Long], rBuf: List[Long]): (Int, Registry, List[Long], List[Long], Boolean) = instructions(ind) match {
      case i @ Snd(r)=>
        (ind + 1, i.run(reg), resolveValue(reg, r) :: sBuf, rBuf, false)
      case Rcv(r)=>
        if (rBuf.nonEmpty)
          (ind + 1, reg.updated(r, rBuf.last), sBuf, rBuf.init, false)
        else (ind, reg, sBuf, rBuf, true)
      case Jgz(c, o) =>
        if (resolveValue(reg, c) > 0) (ind + resolveValue(reg, o).toInt, reg, sBuf, rBuf, false)
        else (ind + 1, reg, sBuf, rBuf, false)
      case i => (ind + 1, i.run(reg), sBuf, rBuf, false)
    }

    def loop(i0: Int, i1: Int, r0: Registry, r1: Registry, b0: List[Long], b1: List[Long]): (Registry, Registry) = {
      if ((i0 < 0 || i0 > instructions.length) && (i1 < 0 || i1 > instructions.length)) (r0, r1)
      else {
        val (ni0, nr0, bb1, bb0, w0) = cycle(i0, r0, b1, b0)
        val (ni1, nr1, bbb0, bbb1, w1) = cycle(i1, r1, bb0, bb1)
        if (w0 && w1 && bbb0.isEmpty && bbb1.isEmpty) (nr0, nr1)
        else loop(ni0, ni1, nr0, nr1, bbb0, bbb1)
      }
    }

    loop(0, 0, initialRegistry.updated("p", 0), initialRegistry.updated("p", 1), List.empty, List.empty)

  }

  println("Part1: " + runProgram(instructions)("lastSnd"))
  println("Part2: " + runPrograms(instructions)._2("snd"))
}
