object Day4 extends App {

  val input =
    """bdwdjjo avricm cjbmj ran lmfsom ivsof
      |mxonybc fndyzzi gmdp gdfyoi inrvhr kpuueel wdpga vkq
      |bneh ylltsc vhryov lsd hmruxy ebnh pdln vdprrky
      |fumay zbccai qymavw zwoove hqpd rcxyvy""".stripMargin

  val lines = input.split("\n").map(_.split(" "))

  def solve(groupingFunction: String => String) = {
    lines.count { words =>
      words.groupBy(groupingFunction).map(_._2.length).max == 1
    }
  }

  println(solve(identity))
  println(solve(_.sorted))
}
