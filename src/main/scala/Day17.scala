
object Day17 extends App {

  val input = 344
  val input1 = 3

  val initial = List(0)

  val part1 = (1 to 2017).foldLeft(initial) { (s, i) =>
    val pos = input % s.length
    if (pos >= s.length) i :: s
    else i :: (s ++ s).drop(pos + 1).take(s.length)
  }

  val (_, after0) = (1 to 50000000).foldLeft((0, 0)) { case ((previous0, after0), i) =>
    val pos = input % i
    if (previous0 == pos) (i, i)
    else if (previous0 < pos)  (i - (pos - previous0), after0)
    else (previous0 - pos, after0)
  }

  println("Part1: " + part1.tail.head)
  println("Part2: " + after0)
}
