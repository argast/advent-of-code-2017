import scala.io.Source

object Day22 extends App {

  sealed abstract class Direction(val dx: Int, val dy: Int) {
    def turnLeft: Direction
    def turnRight: Direction
    def reverse: Direction
  }
  case object Left extends Direction(-1, 0) {
    override def turnLeft: Direction = Down
    override def turnRight: Direction = Up
    override def reverse: Direction = Right
  }
  case object Right extends Direction(1, 0) {
    override def turnLeft: Direction = Up
    override def turnRight: Direction = Down
    override def reverse: Direction = Left
  }
  case object Up extends Direction(0, -1) {
    override def turnLeft: Direction = Left
    override def turnRight: Direction = Right
    override def reverse: Direction = Down
  }
  case object Down extends Direction(0, 1) {
    override def turnLeft: Direction = Right
    override def turnRight: Direction = Left
    override def reverse: Direction = Up
  }

  type Coordinates = (Int, Int)

  val input = Source.fromResource("Day22.txt").mkString

  val input1 =
    """..#
      |#..
      |...""".stripMargin


  def parse(input: String): Map[Coordinates, Int] = {
    val a = input.split("\n").map(_.toCharArray)
    val length = a.length
    (0 until length * length).foldLeft(Map.empty[Coordinates, Int]) { (m, c) =>
      val (x, y) = (c % length, c / length)
      if (a(y)(x) == '#') m.updated((x - length / 2, y - length / 2), 2) else m
    }.withDefaultValue(0)
  }

  val initial = parse(input)

  def printState(state: Map[Coordinates, Int]) = {
    println(state)
  }

  def activity(cycles: Int) = {

    def newDirection(state: Int, d: Direction) = state match {
      case 0 => d.turnLeft
      case 1 => d
      case 2 => d.turnRight
      case _ => d.reverse
    }

    def loop(cycle: Int, x: Int, y: Int, d: Direction, state: Map[Coordinates, Int], infects: Int): Int = {
      if (cycle == cycles) infects
      else {
        val nodeState = state((x, y))
        val newNodeState = nodeState + 1
        val nd = newDirection(nodeState, d)
        val newState = if (newNodeState < 4) state.updated((x, y), newNodeState) else state - ((x, y))
        val newInfects = if (newNodeState == 2) infects + 1 else infects
        loop(cycle + 1, x + nd.dx, y + nd.dy, nd, newState, newInfects)
      }
    }

    loop(0, 0, 0, Up, initial, 0)
  }

  println("Part2: " + activity(10000000))
}
