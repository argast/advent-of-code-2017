import scala.io.Source

object Day21 extends App {

  val input = Source.fromResource("Day21.txt").mkString
  val initial = """.#./..#/###""".stripMargin

  def parse(input: String) = input.split("\n").map { l =>
    val rule = l.split(" => ")
    rule(0) -> rule(1)
  }

  val rules = parse(input).flatMap { case (left, right) => variants(left).map(_ -> right) }.toMap

  def rotate(a: Array[String]): Array[String] = {
    val r = Array.fill(a.length)("")
    for {
      i <- a.indices
      j <- a.indices
    } {
      r(i) += a(a.length - j - 1)(i)
    }
    r
  }

  def variants(rule: String): Vector[String] = {
    val a = rule.split("/")
    val rotatedA = rotate(a)
    val rotated = rotate(a).mkString("/")
    Vector(rule,
      a.reverse.mkString("/"),
      a.map(_.reverse).mkString("/"),
      rule.reverse,
      rotated,
      rotated.reverse,
      rotatedA.reverse.mkString("/"),
      rotatedA.map(_.reverse).mkString("/")
    )
  }

  def nextPicture(current: String): String = {
    val pictureAsArray = current.split("/")
    val div = if (pictureAsArray(0).length % 2 == 0) 2 else 3
    if (pictureAsArray.length > div) {
      val groups = pictureAsArray.size / div
      val windows = for {
        i <- 0 until groups
        j <- 0 until groups
      } yield {
        val window = pictureAsArray.slice(i * div, (i * div) + div).map(_.slice(j * div, j * div + div))
        rules(window.mkString("/")).split("/")
      }

      val windowsCombined = for {
        i <- 0 until groups
      } yield {
        (0 until groups).map(j =>
          windows(i + j * groups)).reduce(_ ++ _)
      }

      windowsCombined(0).indices.map { i =>
        (0 until groups).map(g => windowsCombined(g)(i)).reduce(_ + _)
      }.mkString("/")
    } else rules(current)
  }

  lazy val pictures: Stream[String] = initial #:: pictures.map(nextPicture)

  println("Part1: " + pictures(5).count(_ == '#'))
  println("Part2: " + pictures(18).count(_ == '#'))
}
