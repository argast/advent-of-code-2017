import scala.io.Source

object Day12 extends App {

//  val input = Source.fromResource("Day12.txt").getLines()

  val input =
    """0 <-> 2
      |1 <-> 1
      |2 <-> 0, 3, 4
      |3 <-> 2, 4
      |4 <-> 2, 3, 6
      |5 <-> 6
      |6 <-> 4, 5""".stripMargin.split("\n").toIterator


  case class Input(id: Int, programs: Vector[Int])

  val regex = "(\\d*) <-> (.*)".r

  def parse(input: Iterator[String]) = input.map {
    case regex(id, programs) => Input(id.toInt, programs.split(", ").map(_.toInt).toVector)
  }

  val programsMap = parse(input).map(p => p.id -> p.programs).toMap

  def group(id: Int): Stream[Int] = {
    id #:: programsMap(id).toStream.flatMap(group)
  }

  def solve(id: Int) = {

    def loop(group: Set[Int], toCheck: Set[Int]): Set[Int] = {
      if (toCheck.isEmpty) group
      else {
        val newGroup = group + toCheck.head
        loop(newGroup, (toCheck ++ programsMap(toCheck.head).toSet).diff(newGroup))
      }
    }
    loop(Set(id), programsMap(id).toSet)
  }

  val groups = programsMap.filterKeys(_ != 0).foldLeft(Map(0 -> solve(0))) { case (m, (id, _)) =>
    if (!m.exists(e => e._2.contains(id))) m.updated(id, solve(id)) else m

  }

  println("Part1: " + groups(0).size)
  println("Part2: " + groups.size)
}
