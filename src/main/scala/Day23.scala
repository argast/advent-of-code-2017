import scala.util.Try

object Day23 extends App {

  val input = """set b 81
                |set c b
                |jnz a 2
                |jnz 1 5
                |mul b 100
                |sub b -100000
                |set c b
                |sub c -17000
                |set f 1
                |set d 2
                |set e 2
                |set g d
                |mul g e
                |sub g b
                |jnz g 2
                |set f 0
                |sub e -1
                |set g e
                |sub g b
                |jnz g -8
                |sub d -1
                |set g d
                |sub g b
                |jnz g -13
                |jnz f 2
                |sub h -1
                |set g b
                |sub g c
                |jnz g 2
                |jnz 1 3
                |sub b -17
                |jnz 1 -23""".stripMargin

  val input1 = """set b 81
                 |set c b
                 |jnz a 2
                 |jnz 1 5
                 |mul b 100
                 |sub b -100000
                 |set c b
                 |sub c -17000
                 |set f 1
                 |set d 2
                 |set e 2
                 |set g d
                 |mul g e
                 |sub g b
                 |jnz g 2
                 |set f 0
                 |sub e -1
                 |set g e
                 |sub g b
                 |mul g f
                 |jnz g -9
                 |sub d -1
                 |set g d
                 |sub g b
                 |mul g f
                 |jnz g -15
                 |jnz f 2
                 |sub h -1
                 |set g b
                 |sub g c
                 |jnz g 2
                 |jnz 1 3
                 |sub b -17
                 |jnz 1 -25""".stripMargin

  type Registry = Map[String, Long]

  def resolveValue(r: Registry, s: String) = Try(s.toLong).getOrElse(r(s))

  sealed trait Instruction
  sealed trait RegistryInstruction extends Instruction {
    def run(registry: Registry): Registry
  }

  case class Set(r: String, v: String) extends RegistryInstruction {
    override def run(registry: Registry): Registry = registry.updated(r, resolveValue(registry, v))
  }
  case class Sub(r: String, v: String) extends RegistryInstruction {
    override def run(registry: Registry): Registry = registry.updated(r, registry(r) - resolveValue(registry, v))
  }
  case class Mul(r: String, v: String) extends RegistryInstruction {
    override def run(registry: Registry): Registry = registry.updated(r, registry(r) * resolveValue(registry, v)).updated("mul_no", registry("mul_no") + 1)
  }
  case class Jnz(cond: String, offset: String) extends Instruction

  def parse(input: String): List[Instruction] = input.split("\n").map(_.split(" ") match {
    case Array("set", r, v) => Set(r, v)
    case Array("sub", r, v) => Sub(r, v)
    case Array("mul", r, v) => Mul(r, v)
    case Array("jnz", r, o) => Jnz(r, o)
    case any => throw new Exception("Unknown: " + any.mkString(","))
  }).toList

  val instructions = parse(input)
  val fixedInstructions = parse(input1)
  val initialRegistry: Registry = Map.empty[String, Long].withDefaultValue(0L)

  def runProgram(initialRegistry: Registry, instructions: List[Instruction]): Registry = {

    def loop(ind: Int, reg: Registry): Registry = if (ind < 0 || ind >= instructions.length) reg
    else instructions(ind) match {
      case Jnz(c, o) =>
        if (resolveValue(reg, c) != 0) loop(ind + resolveValue(reg, o).toInt, reg)
        else loop(ind + 1, reg)
      case i: RegistryInstruction => loop(ind + 1, i.run(reg))
    }

    loop(0, initialRegistry)
  }

  println("Part1: " + runProgram(initialRegistry, instructions)("mul_no"))
//  println("Part2: " + runProgram(initialRegistry.updated("a", 1), fixedInstructions)("h"))
  println("Part2: " + part2)

  def part2 = {
    val b = 108100
    val c = b + 17000

    def hasDivider(i: Int) = {
      var j = 1
      do {
        j += 1
      } while (j < i - 1 && i % j != 0)
      i % j == 0
    }

    Range(b, c + 17, 17).map(hasDivider).count(identity)
  }
}
