import scala.collection.mutable

object Day7 extends App {

  val input = """pbga (66)
                |xhth (57)
                |ebii (61)
                |havc (66)
                |ktlj (57)
                |fwft (72) -> ktlj, cntj, xhth
                |qoyq (66)
                |padx (45) -> pbga, havc, qoyq
                |tknk (41) -> ugml, padx, fwft
                |jptl (61)
                |ugml (68) -> gyxo, ebii, jptl
                |gyxo (61)
                |cntj (57)""".stripMargin


  case class Program(name: String, weight: Int, programsAbove: Vector[Program]) {
    lazy val totalWeight: Int = weight + programsAbove.map(_.totalWeight).sum
  }

  val programPattern = "(.*) \\((\\d*)\\)( -> (.*))?".r

  def parse(i: String) = {

    val linesMap = i.split("\n").map(l => l.takeWhile(_ != ' ') -> l).toMap
    val parsedLines = mutable.Map.empty[String, Program]

    def parseProgram(name: String) = parsedLines.getOrElseUpdate(name, parseLine(linesMap(name)))

    def parsePrograms(programs: String) =
      if (programs != null) programs.split(",").map(_.trim).map(parseProgram).toVector
      else Vector.empty

    def parseLine(l: String): Program = l match {
      case programPattern(name, weight, _, subprograms) =>
        parsedLines.getOrElseUpdate(name, Program(name.trim, weight.toInt, parsePrograms(subprograms)))
    }

    linesMap.values.foreach(parseLine)
    parsedLines.toMap
  }

  val programs = parse(input)

  val programsAbove = programs.values.flatMap(_.programsAbove).toSet
  val bottomProgram = programs.values.map(_.name).toSet.diff(programsAbove.map(_.name))

  println("Part1: " + bottomProgram.head)

  def findUneven(program: Program): List[Int] = {

    def changeRequired(p: Program) = {
      if (p.programsAbove.map(_.totalWeight).distinct.size == 2) {
        val sortedByTotal = p.programsAbove.sortBy(-_.totalWeight)
        val bad = sortedByTotal.head
        val good = sortedByTotal(1)
        bad.weight - (bad.totalWeight - good.totalWeight)
      } else 0
    }

    def loop(currentProgram: Program, changesRequired: List[Int]): List[Int] = {
      if (currentProgram.programsAbove.isEmpty) changesRequired
      else {
        val change = changeRequired(currentProgram)
        val newChangesRequired = if (change > 0) change :: changesRequired else changesRequired
        newChangesRequired ++ currentProgram.programsAbove.flatMap(loop(_, changesRequired)).toList
      }
    }

    loop(program, List.empty)
  }

  println("Part2: " + findUneven(programs(bottomProgram.head)).last)

}
