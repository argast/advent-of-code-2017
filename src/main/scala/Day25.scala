//import scala.io.Source
//
//object Day25 extends App {
//
//  val input = Source.fromResource("Day25.txt").mkString
//
//  val input1 =
//    """Begin in state A.
//      |Perform a diagnostic checksum after 6 steps.
//      |
//      |In state A:
//      |  If the current value is 0:
//      |    - Write the value 1.
//      |    - Move one slot to the right.
//      |    - Continue with state B.
//      |  If the current value is 1:
//      |    - Write the value 0.
//      |    - Move one slot to the left.
//      |    - Continue with state B.
//      |
//      |In state B:
//      |  If the current value is 0:
//      |    - Write the value 1.
//      |    - Move one slot to the left.
//      |    - Continue with state A.
//      |  If the current value is 1:
//      |    - Write the value 1.
//      |    - Move one slot to the right.
//      |    - Continue with state A.""".stripMargin
//
//  case class Tape(current: Int, contents: Map[Int, Int])
//
//  case class State(name: String, when0: Tape => (Tape, String), when1: Tape => (Tape, String))
//
//
//
//  def parse(input: String) = input.split("\n").map {
//
//    def parseState(i: Iterator[String]):
//
//  }
//
//  def states = parse()
//}
