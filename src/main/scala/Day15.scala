object Day15 extends App {

  val mask = Long.MaxValue >> 47

  def next(factor: Long, divider: Int = 1)(prev: Long): Long = {
    val nextNumber = prev * factor % 2147483647
    if (nextNumber % divider == 0) nextNumber else next(factor, divider)(nextNumber)
  }

  val initA = 873L
  val initB = 583L

  val nextA1 = next(16807)(_)
  val nextB1 = next(48271)(_)
  val nextA2 = next(16807, 4)(_)
  val nextB2 = next(48271, 8)(_)

  def solve(nextA: Long => Long, nextB: Long => Long, iterations: Int) = {
    (1 to iterations).foldLeft((initA, initB, 0)) { case ((prevA, prevB, count), _) =>
      val na = nextA(prevA)
      val nb = nextB(prevB)
      (na, nb, count + ((na & mask) == (nb & mask)).compareTo(false))
    }
  }

  val result1 = solve(nextA1, nextB1, 40000000)
  val result2 = solve(nextA2, nextB2, 5000000)

  println("Part1: " + result1._3)
  println("Part2: " + result2._3)
}
