

object Day14 extends App {

  implicit class LeftPad(s: String) {
    def leftPadTo(i: Int, c: Char) = String.copyValueOf(Array.fill(i - s.length)(c)) ++ s
  }

  def knotHash(s: String): String = {
    val totalLength = 256
    val initial = (0 until totalLength).toVector
    case class State(index: Int, skip: Int, current: Vector[Int])
    def idx(i: Int) = if (i >= 0) i % 256 else 256 + i

    def reverse(start: Int, length: Int, current: Vector[Int]) = {
      val end = idx(start + length - 1)
      (0 until length).foldLeft(current) { (v, i) =>
        v.updated(idx(end - i), current(idx(start + i)))
      }
    }

    def hash(input: Vector[Int], times: Int) = {
      Vector.fill(times)(input).flatten.foldLeft(State(0, 0, initial)) { case (State(index, skip, current), length) =>
        State(idx(index + length + skip), skip + 1, reverse(index, length, current))
      }
    }

    def compact(input: Vector[Int]) = input.grouped(16).map(_.reduce(_ ^ _)).map(_.toHexString.leftPadTo(2, '0')).mkString

    val input = (s.toCharArray.map(_.toInt) ++ Array(17, 31, 73, 47, 23)).toVector
    compact(hash(input, 64).current)
  }

  case class Drive(input: Seq[String]) {
    private val neighbours = Vector((0, 1), (1, 0), (-1, 0), (0, -1))
    private val contents = input.map(knotHash).map(_.flatMap(c => Integer.parseInt(c.toString, 16).toBinaryString.leftPadTo(4, '0').toCharArray.map(_.asDigit)).toArray).toArray

    def numberOfNonEmpty = contents.flatten.sum
    def numberOfGroups = contents.flatten.distinct.length - 1

    def notEmpty(x: Int, y: Int): Boolean = value(x, y) == 1
    def empty(x: Int, y: Int): Boolean = value(x, y) == 0
    def ungrouped(x: Int, y: Int): Boolean = value(x, y) == 1

    def value(x: Int, y: Int) = contents(x)(y)

    def ungroupedNeighbours(x: Int, y: Int) = neighbours.map { case (xx, yy) => (xx + x, yy + y) }
      .filter { case (x, y) => (x >= 0 && x < 128) && (y >= 0 && y < 128) && ungrouped(x, y)}

    def set(x: Int, y: Int, v: Int) = contents(x).update(y, v)
  }

  val input = (0 to 127).map("vbqugkhl-" + _)
  val input1 = (0 to 127).map("flqrgnkx-" + _)

  val drive = Drive(input)


  def countGroups(input: Seq[String]) = {
    val drive = Drive(input)

    def setGroup(group: Int, toSet: Set[(Int, Int)]): Unit = {
      if (toSet.isEmpty) ()
      else {
        val x = toSet.head._1
        val y = toSet.head._2
        if (drive.ungrouped(x, y))
          drive.set(x, y, group)
        setGroup(group, toSet.tail ++ drive.ungroupedNeighbours(x, y))
      }
    }

    def loop(i: Int, group: Int): Int = {
      if (i == 128 * 128) group - 2
      else {
        val x = i / 128
        val y = i % 128
        if (drive.notEmpty(x, y)) {
          setGroup(group, Set((x, y)))
          loop(i + 1, group + 1)
        } else {
          loop(i + 1, group)
        }
      }
    }

    loop(0, 2)
  }

  println("Part1: " + drive.numberOfNonEmpty)
  println("Part2: " + countGroups(input))
}
