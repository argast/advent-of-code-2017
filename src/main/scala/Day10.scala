object Day10 extends App {

  implicit class LeftPad(s: String) {
    def leftPadTo(i: Int, c: Char) = String.copyValueOf(Array.fill(i - s.length)(c)) ++ s
  }

  val input = "199,0,255,136,174,254,227,16,51,85,1,2,22,17,7,192".split(",").map(_.toInt).toVector
  val inputPart2 = "".toCharArray.map(_.toInt).toVector ++ Vector(17, 31, 73, 47, 23)
  //val inputPart2 = "199,0,255,136,174,254,227,16,51,85,1,2,22,17,7,192".toCharArray.map(_.toInt).toVector ++ Vector(17, 31, 73, 47, 23)

  val totalLength = 256

  val initial = (0 until totalLength).toVector

  def idx(i: Int) = if (i >= 0) i % totalLength else totalLength + i

  case class State(index: Int, skip: Int, current: Vector[Int])

  def reverse(start: Int, length: Int, current: Vector[Int]) = {
    val end = idx(start + length - 1)
    (0 until length).foldLeft(current) { (v, i) =>
      v.updated(idx(end - i), current(idx(start + i)))
    }
  }

  def hash(input: Vector[Int], times: Int) = {
    Vector.fill(times)(input).flatten.foldLeft(State(0, 0, initial)) { case (s @ State(index, skip, current), length) =>
      State(idx(index + length + skip), skip + 1, reverse(index, length, current))
    }
  }

  def compact(input: Vector[Int]) = input.grouped(16).map(_.reduce(_ ^ _)).map(_.toHexString.leftPadTo(2, '0')).mkString

    //val result = hash(input, 1)
  val result2 = hash(inputPart2, 64)

  println(result2.current.grouped(16).toVector.map(_.reduce(_ ^ _)))

  //println("Part1: " + (result.current(0) * result.current(1)))
  println("Part2: " + compact(result2.current))

}
