import scala.io.Source

object Day5 extends App {

  val input =  Source.fromResource("Day5.txt").mkString.split("\n").map(_.toInt).toList
  val input1 =
    """0
      |3
      |0
      |1
      |-3""".stripMargin.split("\n").map(_.toInt).toList

  case class Step(position: Int, instructions: Array[Int])

  def solve(input: List[Int], offsetFunction: Int => Int): Int = {

    def loop(s: Step, i: Int): Int = s match {
      case Step(position, instructions) =>
        val offset = instructions(position)
        val newPosition = position + offset
        instructions(position) = instructions(position) + offsetFunction(offset)
        if (newPosition >= 0 && newPosition < input.length)
          loop(Step(newPosition, instructions), i + 1)
        else i + 1
    }

    loop(Step(0, input.toArray), 0)
  }

  val part1: Int => Int = _ => 1
  val part2: Int => Int = offset => if (offset >= 3) -1 else 1
  println(solve(input, part1))
  println(solve(input, part2))
}
