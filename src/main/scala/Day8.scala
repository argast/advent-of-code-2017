object Day8 extends App {

  val input =
    """b inc 5 if a > 1
      |a inc 1 if b < 5
      |c dec -10 if a >= 1
      |c inc -20 if c == 10""".stripMargin

  type Op = (Int, Int) => Int
  type Comp = (Int, Int) => Boolean
  type Memory = Map[String, Int]
  case class Condition(v: String, f: Comp, n: Int) {
    def run(mem: Memory) = f (mem(v), n)
  }
  case class Instruction(v: String, op: Op, n: Int, condition: Condition) {
    def run(mem: Memory) = if (condition.run(mem)) mem.updated(v, op(mem(v), n)) else mem
  }

  val linePattern = "(.*) (inc|dec) (-?\\d*) (.*)".r
  val condPattern = "if (.*) (.*) (-?\\d*)".r

  def opMap: Map[String, Op] = Map(
    "inc" -> (_ + _),
    "dec" -> (_ - _)
  )

  def compMap: Map[String, Comp] = Map(
    ">" -> (_ > _),
    "<" -> (_ < _),
    ">=" -> (_ >= _),
    "<=" -> (_ <= _),
    "==" -> (_ == _),
    "!=" -> (_ != _)
  )

  def parse(input: String) = {

    def parseCondition(c: String): Condition = c match {
      case condPattern(v, comp, n) => Condition(v, compMap(comp), n.toInt)
    }

    def parseLine(line: String) = line match {
      case linePattern(v, op, n, cond) => Instruction(v, opMap(op), n.toInt, parseCondition(cond))
    }
    input.split("\n").map(parseLine)
  }

  val instructions = parse(input)
  val memory: Memory = Map.empty.withDefaultValue(0)

  val memoryStates = instructions.scanLeft(memory) { (mem, instruction) =>
    instruction.run(mem)
  }.toVector

  println("Part1: " + memoryStates.last.maxBy(_._2))
  println("Part2: " + memoryStates.filterNot(_.isEmpty).map(_.maxBy(_._2)).maxBy(_._2))

}
