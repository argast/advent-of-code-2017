object Day6 extends App {

  val input = "0\t5\t10\t0\t11\t14\t13\t4\t11\t8\t8\t7\t1\t4\t12\t11".split("\t").map(_.toInt).toVector

  def wrapIndex(i: Int) = i % input.length

  def redistribute(input: Vector[Int]): Vector[Int] = {
    val (max, maxPosition) = input.zipWithIndex.maxBy(_._1)
    (1 to max).foldLeft(input.updated(maxPosition, 0)) { (currentInput, i) =>
      currentInput.updated(wrapIndex(maxPosition + i), currentInput(wrapIndex(maxPosition + i)) + 1)
    }
  }

  def solve(input: Vector[Int]) = {

    def loop(currentInput: Vector[Int], previous: Set[String]): (Vector[Int], Int) = {
      if (previous.contains(currentInput.mkString)) currentInput -> previous.size
      else loop(redistribute(currentInput), previous + currentInput.mkString)
    }

    loop(input, Set.empty)
  }

  val (lastState1, iterations1) = solve(input)
  val (_, iterations2) = solve(lastState1)
  println("Part1: " + iterations1)
  println("Part2: " + iterations2)
}
